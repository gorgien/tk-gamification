package com.textkernel.gamification.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import com.textkernel.gamification.constant.EventTypes;
import com.textkernel.gamification.model.Action;
import com.textkernel.gamification.model.Badge;
import com.textkernel.gamification.model.EarnedBadge;
import com.textkernel.gamification.model.Event;
import com.textkernel.gamification.model.UserBadges;

@Component
public class UserService {
	@Autowired
	private MongoTemplate mongoTemplate;

	public void addAction(String userId, String actionName) {
		Action action = new Action(userId, actionName);
		action.setDate(Calendar.getInstance().getTime());

		mongoTemplate.save(action);
	}

	public Event addEvent(String userId, EventTypes eventType) {
		Query searchUserQuery = new Query(Criteria.where("userId").is(userId).and("event").is(eventType.name()));
		Event event = mongoTemplate.findOne(searchUserQuery, Event.class);

		if (event == null) {
			event = new Event(userId, eventType);
		}

		event.incrementCount();
		event.setLastEventDate(Calendar.getInstance().getTime());

		mongoTemplate.save(event);

		return event;
	}

	public List<EarnedBadge> getBadges(String userId) {
		Query searchUserQuery = new Query(Criteria.where("userId").is(userId));
		UserBadges userBadges = mongoTemplate.findOne(searchUserQuery, UserBadges.class);

		if (userBadges == null) {
			return new ArrayList<EarnedBadge>();
		}
		return userBadges.getBadges();
	}

	public List<Action> getActions(String userId) {
		Query searchUserQuery = new Query(Criteria.where("userId").is(userId));
		return mongoTemplate.find(searchUserQuery, Action.class);
	}

	public List<Event> getEvents(String userId) {
		Query searchUserQuery = new Query(Criteria.where("userId").is(userId));
		return mongoTemplate.find(searchUserQuery, Event.class);
	}

	public void addBadge(String userId, Badge badge) {
		Query searchUserQuery = new Query(Criteria.where("userId").is(userId));
		UserBadges userBadge = mongoTemplate.findOne(searchUserQuery, UserBadges.class);

		if (userBadge == null) {
			userBadge = new UserBadges(userId);
		}

		final EarnedBadge earnedBadge = new EarnedBadge(Calendar.getInstance().getTime(), badge.getBadge());
		userBadge.addBadge(earnedBadge);

		mongoTemplate.save(userBadge);
	}

	public void clearActions() {
		mongoTemplate.dropCollection(Action.class);
	}

	public void clearEvents() {
		mongoTemplate.dropCollection(Event.class);
	}

	public void clearUserBadges() {
		mongoTemplate.dropCollection(UserBadges.class);
	}
}
