package com.textkernel.gamification.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;

import com.textkernel.gamification.model.Badge;

@Component
public class BadgeService {
	@Autowired
	private MongoTemplate mongoTemplate;

	public void saveBadge(Badge badge) {
		mongoTemplate.save(badge);
	}

	public void clearBadges() {
		mongoTemplate.dropCollection(Badge.class);
	}

	public List<Badge> getBadges() {
		return mongoTemplate.findAll(Badge.class);
	}
}
