package com.textkernel.gamification.rest.request;

public class ActionRequest {

	private String userId;
	private String actionName;

	public ActionRequest() {
		super();
	}

	public ActionRequest(String userId, String actionName) {
		super();
		this.userId = userId;
		this.actionName = actionName;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getActionName() {
		return actionName;
	}

	public void setActionName(String actionName) {
		this.actionName = actionName;
	}

}
