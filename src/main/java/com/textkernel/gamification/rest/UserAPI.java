package com.textkernel.gamification.rest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.textkernel.gamification.constant.ActionTypes;
import com.textkernel.gamification.constant.BadgeTypes;
import com.textkernel.gamification.constant.EventTypes;
import com.textkernel.gamification.constant.Status;
import com.textkernel.gamification.model.Action;
import com.textkernel.gamification.model.Badge;
import com.textkernel.gamification.model.BadgeRequirement;
import com.textkernel.gamification.model.EarnedBadge;
import com.textkernel.gamification.model.Event;
import com.textkernel.gamification.rest.request.ActionRequest;
import com.textkernel.gamification.rest.response.Response;
import com.textkernel.gamification.service.BadgeService;
import com.textkernel.gamification.service.UserService;

@RestController
@RequestMapping(value = "/userService/")
public class UserAPI {

	@Autowired
	private UserService userService;

	@Autowired
	private BadgeService badgeService;

	@CrossOrigin
	@RequestMapping(value = "test/", method = RequestMethod.GET)
	public String test() {
		return "OK";
	}

	@CrossOrigin
	@RequestMapping(value = "generateBadgeRequirements/", method = RequestMethod.GET)
	public Response<List<Badge>> generateBadgeRequirements() {

		Response<List<Badge>> response = new Response<>();
		badgeService.clearBadges();

		badgeService.saveBadge(new Badge(BadgeTypes.TIME_SAVER, new BadgeRequirement(EventTypes.TIMESAVER, 3)));
		badgeService.saveBadge(new Badge(BadgeTypes.QUERY_HERO, new BadgeRequirement(EventTypes.QUERY, 4)));

		response.setStatus(Status.OK);
		response.setData(badgeService.getBadges());

		return response;
	}

	@CrossOrigin
	@RequestMapping(value = "addAction/", method = RequestMethod.POST)
	public Response<List<Badge>> addAction(@RequestBody ActionRequest request) {

		Response<List<Badge>> response = new Response<>();

		final ActionTypes action = ActionTypes.fromName(request.getActionName());
		if (action == null) {
			response.setStatus(Status.ERROR);
			return response;
		}

		final String userId = request.getUserId();

		userService.addAction(userId, request.getActionName());
		Event event = userService.addEvent(userId, action.getRelatedEvent());

		final List<Badge> badges = badgeService.getBadges();
		final List<EarnedBadge> userBadges = userService.getBadges(userId);

		List<Badge> newBadges = new ArrayList<>();
		for (Badge badge : badges) {
			if (userBadges.contains(new EarnedBadge(null, badge.getBadge()))) {
				continue;
			}

			boolean satisfyRequirement = true;
			final List<BadgeRequirement> requirements = badge.getRequirements();
			for (BadgeRequirement badgeRequirement : requirements) {
				if (!event.getEvent().equals(badgeRequirement.getEvent().name())
						|| badgeRequirement.getCount() > event.getCount()) {
					satisfyRequirement = false;
				}
			}

			if (satisfyRequirement) {
				userService.addBadge(userId, badge);
				newBadges.add(badge);
			}
		}

		response.setStatus(Status.OK);
		response.setData(newBadges);

		return response;
	}

	@CrossOrigin
	@RequestMapping(value = "{userId}/badges", produces = "application/json", method = RequestMethod.GET)
	public Response<List<EarnedBadge>> getBadges(@PathVariable("userId") String userId) {
		Response<List<EarnedBadge>> response = new Response<>();

		response.setStatus(Status.OK);
		final List<EarnedBadge> badges = userService.getBadges(userId);

		Collections.sort(badges, new Comparator<EarnedBadge>() {

			@Override
			public int compare(EarnedBadge o1, EarnedBadge o2) {
				return o2.getEarnDate().compareTo(o1.getEarnDate());
			}
		});

		response.setData(badges);
		return response;
	}

	@CrossOrigin
	@RequestMapping(value = "{userId}/events", produces = "application/json", method = RequestMethod.GET)
	public Response<List<Event>> getEvents(@PathVariable("userId") String userId) {
		Response<List<Event>> response = new Response<>();

		response.setStatus(Status.OK);
		response.setData(userService.getEvents(userId));
		return response;
	}

	@CrossOrigin
	@RequestMapping(value = "{userId}/actions", produces = "application/json", method = RequestMethod.GET)
	public Response<List<Action>> getActions(@PathVariable("userId") String userId) {
		Response<List<Action>> response = new Response<>();

		response.setStatus(Status.OK);
		response.setData(userService.getActions(userId));
		return response;
	}

	@CrossOrigin
	@RequestMapping(value = "reset/", method = RequestMethod.GET)
	public Response<String> reset() {

		Response<String> response = new Response<>();
		userService.clearActions();
		userService.clearEvents();
		userService.clearUserBadges();

		response.setStatus(Status.OK);
		response.setData("Reset");

		return response;
	}
}
