package com.textkernel.gamification.model;

import java.util.Date;

import org.springframework.data.annotation.Id;

public class Action {

	@Id
	private String id;
	private String userId;
	private String action;
	private Date date;

	public Action() {
		super();
	}

	public Action(String userId, String actionName) {
		super();
		this.userId = userId;
		this.action = actionName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "Action [userId=" + userId + ", action=" + action + ", date=" + date + "]";
	}
}
