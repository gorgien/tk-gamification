package com.textkernel.gamification.model;

import com.textkernel.gamification.constant.EventTypes;

public class BadgeRequirement {

	private EventTypes event;
	private int count;

	public BadgeRequirement() {
		super();
	}

	public BadgeRequirement(EventTypes event, int count) {
		super();
		this.event = event;
		this.count = count;
	}

	public EventTypes getEvent() {
		return event;
	}

	public void setEvent(EventTypes event) {
		this.event = event;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	@Override
	public String toString() {
		return "BadgeRequirement [event=" + event + ", count=" + count + "]";
	}
}
