package com.textkernel.gamification.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.textkernel.gamification.constant.BadgeTypes;

@Document(collection = "badge")
public class Badge extends BadgeType {

	@Id
	private String id;

	private List<BadgeRequirement> requirements;

	public Badge() {
		super();
		this.requirements = new ArrayList<>();
	}

	public Badge(BadgeTypes badge, BadgeRequirement requirement) {
		super();
		this.badge = badge;
		this.requirements = new ArrayList<>();
		this.requirements.add(requirement);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<BadgeRequirement> getRequirements() {
		return requirements;
	}

	public void setRequirements(List<BadgeRequirement> requirements) {
		this.requirements = requirements;
	}

	@Override
	public String toString() {
		return "Badge [badge=" + badge + ", requirements=" + requirements + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((badge == null) ? 0 : badge.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Badge other = (Badge) obj;
		if (badge != other.badge)
			return false;
		return true;
	}

}
