package com.textkernel.gamification.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "user_badge")
public class UserBadges {

	@Id
	private String id;

	private String userId;
	private List<EarnedBadge> badges;

	public UserBadges() {
		super();
		badges = new ArrayList<EarnedBadge>();
	}

	public UserBadges(String userId) {
		super();
		this.userId = userId;
		badges = new ArrayList<EarnedBadge>();
	}

	public UserBadges(String userId, List<EarnedBadge> badges) {
		super();
		this.userId = userId;
		this.badges = badges;
	}

	public void addBadge(EarnedBadge earnedBadge) {
		this.badges.add(earnedBadge);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public List<EarnedBadge> getBadges() {
		return badges;
	}

	public void setBadges(List<EarnedBadge> badges) {
		this.badges = badges;
	}
}
