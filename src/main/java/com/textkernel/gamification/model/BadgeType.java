package com.textkernel.gamification.model;

import com.textkernel.gamification.constant.BadgeTypes;

public class BadgeType {
	protected BadgeTypes badge;

	public BadgeTypes getBadge() {
		return badge;
	}

	public String getBadgeName() {
		return badge.getName();
	}

	public String getBadgeSrc() {
		return badge.getSrc();
	}

	public void setBadge(BadgeTypes badge) {
		this.badge = badge;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((badge == null) ? 0 : badge.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BadgeType other = (BadgeType) obj;
		if (badge != other.badge)
			return false;
		return true;
	}

}
