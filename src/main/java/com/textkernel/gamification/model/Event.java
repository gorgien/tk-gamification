package com.textkernel.gamification.model;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.textkernel.gamification.constant.EventTypes;

@Document(collection = "event")
public class Event {

	@Id
	private String id;

	private String userId;
	private String event;
	private Date lastEventDate;
	private int count;

	public Event() {
		super();
		this.count = 0;
	}

	public Event(String userId, EventTypes event) {
		super();
		this.userId = userId;
		this.event = event.name();
		this.count = 0;
	}

	public void incrementCount() {
		this.count++;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public Date getLastEventDate() {
		return lastEventDate;
	}

	public void setLastEventDate(Date lastEventDate) {
		this.lastEventDate = lastEventDate;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	@Override
	public String toString() {
		return "Event [id=" + id + ", userId=" + userId + ", event=" + event + ", lastEventDate=" + lastEventDate + ", count=" + count
				+ "]";
	}
}
