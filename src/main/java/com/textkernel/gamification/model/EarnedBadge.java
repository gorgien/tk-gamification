package com.textkernel.gamification.model;

import java.util.Date;

import com.textkernel.gamification.constant.BadgeTypes;

public class EarnedBadge extends BadgeType {

	private Date earnDate;

	public EarnedBadge() {
		super();
	}

	public EarnedBadge(Date earnDate, BadgeTypes badge) {
		super();
		this.earnDate = earnDate;
		this.badge = badge;
	}

	public Date getEarnDate() {
		return earnDate;
	}

	public void setEarnDate(Date earnDate) {
		this.earnDate = earnDate;
	}
}
