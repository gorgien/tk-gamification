package com.textkernel.gamification.constant;

public enum BadgeTypes {

	TIME_SAVER("Time Saver","1_2_TimeSaver"),
	QUERY_HERO("Query Hero","1_1_QueryHero");
	
	private String name;
	private String src;
	
	private BadgeTypes(String name, String src) {
		this.name = name;
		this.src = src;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSrc() {
		return src;
	}

	public void setSrc(String src) {
		this.src = src;
	}
}
