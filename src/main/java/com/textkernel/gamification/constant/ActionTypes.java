package com.textkernel.gamification.constant;

import java.util.HashMap;
import java.util.Map;

public enum ActionTypes {
	ADD_ITEM("add_item", EventTypes.QUERY),
	REMOVE_ITEM("remove_item", EventTypes.QUERY),
	CHECK_SYNONYM("check_synonym", EventTypes.TIMESAVER),
	RADIO_BUTTON("radio_change", EventTypes.TIMESAVER);

	private String actionName;
	private EventTypes relatedEvent;
	private static Map<String, ActionTypes> map;

	static {
		map = new HashMap<>();

		final ActionTypes[] values = ActionTypes.values();
		for (ActionTypes action : values) {
			map.put(action.actionName, action);
		}
	}

	private ActionTypes(String actionName, EventTypes relatedEvent) {
		this.actionName = actionName;
		this.relatedEvent = relatedEvent;
	}

	public static ActionTypes fromName(String name) {
		return map.get(name);
	}

	public String getActionName() {
		return actionName;
	}

	public void setActionName(String actionName) {
		this.actionName = actionName;
	}

	public EventTypes getRelatedEvent() {
		return relatedEvent;
	}

	public void setRelatedEvent(EventTypes relatedEvent) {
		this.relatedEvent = relatedEvent;
	}
}
